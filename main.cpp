#include "CAN.h"
#include "mbed.h"

static BufferedSerial pc(USBTX, USBRX, 115200);
CAN can_interface(D10, D2);

int set_pos = 0;
int set_vel = 0;
int set_tff = 0;
int set_kp  = 0;
int set_kd  = 0;

void transmitMsg(int position, int velocity, int ff, int kp, int kd) {
    CANMessage msg;
    // convert all the floats to uint
    // position
    msg.data[0] = position >> 8;   // 8 bit
    msg.data[1] = position & 0xff; // 8 bit

    // velocity
    msg.data[2] = velocity >> 4;         // 8 bit
    msg.data[3] = (velocity & 0xf) << 4; // 4 bit

    // kp
    msg.data[3] = (msg.data[3] | kp >> 8); // 4 bit by kp
    msg.data[4] = kp & 0xff;               // 8 bit by kp

    // kd
    msg.data[5] = kd >> 4;         // 8 bit
    msg.data[6] = (kd & 0xf) << 4; // 4 bit

    // ff
    msg.data[6] = (msg.data[6] | ff >> 8); // 4 bit by kp
    msg.data[7] = ff & 0xff;               // 8 bit by kp

    if (can_interface.write(CANMessage(1, msg.data, 8, CANData, CANStandard))) {
    }
}

int float_to_uint(float x, float x_min, float x_max, int bits) {
    /// Converts a float to an unsigned int, given range and number of bits ///
    float span = x_max - x_min;
    float offset = x_min;
    return (int)((x - offset) * ((float)((1 << bits) - 1)) / span);
}

void interpret_command(char *cmd) {
    char desired_param = ' ';
    float desired_value_1 = NAN;
    float desired_value_2 = NAN;

    int num_parsed = sscanf(cmd, " %c %f %f", &desired_param, &desired_value_1, &desired_value_2);

    switch (desired_param) {
        case 'p':
            set_pos = float_to_uint(desired_value_1, -95.5f,  95.5f, 16);
            printf("Set p: %d\n", set_pos);
            break;
        case 'v':
            set_vel = float_to_uint(desired_value_1, -45.0f,  45.0f, 12);
            printf("Set v: %d\n", set_vel);
            break;
        case 't':
            set_tff = float_to_uint(desired_value_1, -18.0f,  18.0f, 12);
            printf("Set t: %d\n", set_tff);
            break;
        case 'k':
            set_kp  = float_to_uint(desired_value_1,   0.0f, 500.0f,  8);
            set_kd  = float_to_uint(desired_value_2,   0.0f,   5.0f,  8);
            printf("Set Kp: %d Kd: %d\n", set_kp, set_kd);
            break;
        default:
            break;
    }

    printf("\n\tp: %d\n\tv: %d\n\tt: %d\n\tKp: %d\n\tKd: %d\n\n", set_pos, set_vel, set_tff, set_kp, set_kd);
    transmitMsg(set_pos, set_vel, set_tff, set_kp, set_kd);
}

int main() {
    printf("Hello World!\n");
    can_interface.frequency(1000000);

    set_pos = float_to_uint(  0.0f, -95.5f,  95.5f, 16);
    set_vel = float_to_uint(  0.0f, -45.0f,  45.0f, 12);
    set_tff = float_to_uint(  0.0f, -18.0f,  18.0f, 12);
    set_kp  = float_to_uint( 40.0f, 0.0f, 500.0f, 8);
    set_kd  = float_to_uint(  0.0f, 0.0f,   5.0f, 8);

    printf("\n\tp: %d\n\tv: %d\n\tt: %d\n\tKp: %d\n\tKd: %d\n\n", set_pos, set_vel, set_tff, set_kp, set_kd);

    uint8_t motor_mode[8] = {
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfc
    }; // enters motor mode/closed loop control
    uint8_t exit_mode[8] = {
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd
    }; // exits the closed loop control
    uint8_t zero_mode[8] = {
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfe
    }; // set current position to zero

    if (can_interface.write(
            CANMessage(1, exit_mode, 8, CANData, CANStandard))) {
    }
    ThisThread::sleep_for(5ms);
    if (can_interface.write(
            CANMessage(1, zero_mode, 8, CANData, CANStandard))) {
    }
    ThisThread::sleep_for(5ms);
    if (can_interface.write(
            CANMessage(1, motor_mode, 8, CANData, CANStandard))) {
    }
    ThisThread::sleep_for(5ms);

    char c[1];
    char cmd[32];
    int pos = 0;

    while (1) {
        pc.read(c, sizeof(c));
        if (*c == ';' || *c == '\n') {
            cmd[pos] = '\0';
            interpret_command(cmd);
            pos = 0;
        } else {
            cmd[pos++] = *c;
        }
    }
}